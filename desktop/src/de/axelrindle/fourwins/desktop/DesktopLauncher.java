package de.axelrindle.fourwins.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import de.axelrindle.fourwins.MyGame;

import java.awt.*;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.fullscreen = !MyGame.isDebug();
		config.vSyncEnabled = true;
		config.useGL30 = true;
		config.title = "4 Gewinnt";

		DisplayMode mode = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
		config.width = mode.getWidth();
		config.height = mode.getHeight();

		new LwjglApplication(new MyGame(), config);
	}
}
