package de.axelrindle.fourwins;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import de.axelrindle.fourwins.screen.LoadingScreen;

import java.util.Objects;

public class MyGame extends Game {

	public static boolean isDebug() {
		return Objects.equals(System.getenv("GAME_ENV"), "debug");
	}

	private SpriteBatch batch;
	private Skin skin;

	private AssetManager assetManager = new AssetManager();

	@Override
	public void create() {
		batch = new SpriteBatch();
		skin = new Skin(Gdx.files.internal("skin.json"));

		prepareAssetsToLoad();

		setScreen(new LoadingScreen(this));
	}

	private void prepareAssetsToLoad() {
		assetManager.load("adamgames.png", Texture.class);
		assetManager.load("badlogic.jpg", Texture.class);
		assetManager.load("libgdx.png", Texture.class);

		assetManager.load("ui/arcs1.png", Texture.class);
		assetManager.load("ui/arcs2.png", Texture.class);

		// Objects
		assetManager.load("board/slot.png", Texture.class);
		assetManager.load("board/chip_red.png", Texture.class);
		assetManager.load("board/chip_yellow.png", Texture.class);
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose() {
		screen.dispose();
        batch.dispose();
        skin.dispose();
        assetManager.dispose();
    }

	public SpriteBatch getBatch() {
		return batch;
	}

	public Skin getSkin() {
		return skin;
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}
}
