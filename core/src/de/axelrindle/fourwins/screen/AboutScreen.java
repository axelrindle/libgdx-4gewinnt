package de.axelrindle.fourwins.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import de.axelrindle.fourwins.MyGame;
import de.axelrindle.fourwins.MyUtils;

public class AboutScreen extends BasicScreen {

    private static final String ABOUT =
            "Ein einfaches Spiel, um die Mechaniken von LibGDX zu verstehen und " +
            "das Entwickeln von Spielen damit zu lernen.";

    private Label title;
    private Label aboutText;
    private Label poweredByText;
    private TextButton buttonMenu;

    private SpriteBatch batch;
    private Texture badlogicLogo;
    private Texture libgdxLogo;

    public AboutScreen(MyGame game) {
        super(game);
    }

    @Override
    protected void create() {
        title = new Label("4 Gewinnt", game.getSkin(), "title");
        aboutText = new Label(ABOUT, game.getSkin());
        aboutText.setAlignment(Align.center);
        aboutText.setWrap(true);
        poweredByText = new Label("Powered by", game.getSkin());

        batch = new SpriteBatch();
        badlogicLogo = game.getAssetManager().get("badlogic.jpg", Texture.class);
        libgdxLogo = game.getAssetManager().get("libgdx.png", Texture.class);

        buttonMenu = new TextButton("Menü", game.getSkin());
        buttonMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });
        MyUtils.fixActorSize(buttonMenu);

        stage.addActor(title);
        stage.addActor(aboutText);
        stage.addActor(poweredByText);
        stage.addActor(buttonMenu);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        batch.setProjectionMatrix(stage.getCamera().combined);
        batch.begin();
        batch.draw(badlogicLogo, 25, 25);
        batch.draw(libgdxLogo, (Gdx.graphics.getWidth() - libgdxLogo.getWidth()) / 2f, 25);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        title.setPosition(
                (width - title.getWidth()) / 2,
                height - title.getHeight() * 5
        );

        aboutText.setWidth(Gdx.graphics.getWidth() / 1.5f);
        aboutText.setHeight(aboutText.getHeight() * 3);
        aboutText.setPosition(
                (width - aboutText.getWidth()) / 2,
                (height - aboutText.getHeight()) / 2
        );

        poweredByText.setPosition(
                (width - poweredByText.getWidth()) / 2,
                libgdxLogo.getHeight() + poweredByText.getHeight() + 25
        );

        buttonMenu.setPosition(
                width - buttonMenu.getWidth() - 25,
                25
        );
    }

    @Override
    public void dispose() {
        batch.dispose();
        super.dispose();
    }
}
