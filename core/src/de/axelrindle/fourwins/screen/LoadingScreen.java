package de.axelrindle.fourwins.screen;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.utils.Timer;
import de.axelrindle.fourwins.MyGame;

public class LoadingScreen extends BasicScreen {

    private Image logo;
    private ProgressBar progressBar;
    private Label percentage;

    public LoadingScreen(MyGame game) {
        super(game);
    }

    @Override
    protected void create() {
        logo = new Image(new Texture("adamgames.png"));
        progressBar = new ProgressBar(0, 100, 1, false, game.getSkin());
        percentage = new Label("", game.getSkin());

        stage.addActor(logo);
        stage.addActor(progressBar);
        stage.addActor(percentage);
    }

    private boolean scheduled;

    @Override
    public void render(float delta) {
        super.render(delta);

        // when done, switch to menu after 2 seconds
        if (progressBar.getValue() == 100 && !scheduled) {
            scheduled = true;
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    game.setScreen(new MenuScreen(game));
                }
            }, 2);
        }

        // update progress bar
        float progress = game.getAssetManager().getProgress() * 100;
        progressBar.setValue(progress);

        // progressively load assets
        if (game.getAssetManager().update()) {
            percentage.setText("Loading finished.");
            doResize();
        } else {
            percentage.setText(Math.round(progress) + "%");
        }
    }

    @Override
    public void resize(int screenWidth, int screenHeight) {
        super.resize(screenWidth, screenHeight);
        progressBar.setWidth(screenWidth / 1.5f);
        progressBar.setPosition(
                (screenWidth - progressBar.getWidth()) / 2,
                progressBar.getHeight() * 2.5f
        );

        logo.setPosition(
                (screenWidth - logo.getWidth()) / 2,
                (screenHeight - logo.getHeight() + progressBar.getY()) / 2
        );

        percentage.setPosition(
                (screenWidth - percentage.getPrefWidth()) / 2,
                progressBar.getY() - percentage.getHeight() - 35
        );
    }
}
