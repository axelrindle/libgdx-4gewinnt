package de.axelrindle.fourwins.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.axelrindle.fourwins.MyGame;
import de.axelrindle.fourwins.MyUtils;

@SuppressWarnings("FieldCanBeLocal")
public class MenuScreen extends BasicScreen {

    private Label title;

    private Label authors;
    private Label version;

    private HorizontalGroup buttonGroup;
    private TextButton buttonPlay;
    private TextButton buttonSettings;
    private TextButton buttonAbout;
    private TextButton buttonQuit;

    public MenuScreen(MyGame game) {
        super(game);
    }

    @Override
    protected void create() {
        title = new Label("4 Gewinnt", game.getSkin(), "title");
        authors = new Label("Ein Projekt von Axel", game.getSkin());
        authors.setColor(Color.GRAY);
        version = new Label("@VERSION@", game.getSkin());
        version.setColor(Color.GRAY);

        buttonPlay = new TextButton("Spielen", game.getSkin());
        buttonPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game));
            }
        });

        buttonSettings = new TextButton("Einstellungen", game.getSkin());
        buttonSettings.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new SettingsScreen(game));
            }
        });

        buttonAbout = new TextButton("Über", game.getSkin());
        buttonAbout.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new AboutScreen(game));
            }
        });

        buttonQuit = new TextButton("Beenden", game.getSkin());
        buttonQuit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });

        buttonGroup = new HorizontalGroup();
        buttonGroup.addActor(buttonPlay);
        buttonGroup.space(25);
        buttonGroup.addActor(buttonSettings);
        buttonGroup.space(25);
        buttonGroup.addActor(buttonQuit);

        buttonPlay.pad(25);
        buttonSettings.pad(25);
        buttonQuit.pad(25);
        MyUtils.fixActorSize(buttonAbout);

        stage.addActor(title);
        stage.addActor(authors);
        stage.addActor(version);
        stage.addActor(buttonGroup);
        stage.addActor(buttonAbout);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        title.setPosition(
                (width - title.getWidth()) / 2,
                height - title.getHeight() * 5
        );

        authors.setPosition(25, authors.getHeight() + 15);
        version.setPosition(
                width - version.getWidth() - 25,
                version.getHeight() + 15
        );

        buttonGroup.setPosition(
                (width - buttonGroup.getPrefWidth()) / 2,
                (height - buttonGroup.getPrefHeight()) / 2
        );
        buttonAbout.setPosition(
                (width - buttonAbout.getWidth()) / 2,
                buttonGroup.getY() - buttonAbout.getHeight() - 75
        );
    }
}
