package de.axelrindle.fourwins.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import de.axelrindle.fourwins.MyGame;
import org.jetbrains.annotations.NotNull;

public abstract class BasicScreen extends ScreenAdapter {

    protected MyGame game;
    protected Stage stage;

    public BasicScreen(@NotNull MyGame game) {
        this.game = game;

        // create stage
        stage = new Stage(new ScreenViewport(), game.getBatch());
        Gdx.input.setInputProcessor(stage);

        // debug drawing
        if (MyGame.isDebug()) {
            stage.addListener(new InputListener() {
                @Override
                public boolean keyUp(InputEvent event, int keycode) {
                    if (keycode == Input.Keys.BACKSLASH) {
                        stage.setDebugAll(!stage.isDebugAll());
                    }
                    return false;
                }
            });
        }

        create();
    }

    protected abstract void create();

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    protected void doResize() {
        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();
        resize(screenWidth, screenHeight);
    }

    protected void packWidgets() {
        boolean changed = false;
        for (Actor actor : stage.getActors()) {
            if (actor instanceof Widget) {
                Widget w = (Widget) actor;
                if (w.needsLayout()) {
                    w.pack();
                    changed = true;
                }
            }
        }
        if (changed) doResize();
    }
}
