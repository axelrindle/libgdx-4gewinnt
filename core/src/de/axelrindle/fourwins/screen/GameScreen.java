package de.axelrindle.fourwins.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import de.axelrindle.fourwins.MyGame;
import de.axelrindle.fourwins.MyUtils;
import de.axelrindle.fourwins.WinningLine;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.stream.IntStream;

public class GameScreen extends BasicScreen {

    private final int cols = 8;
    private final int rows = 8;

    private int currentCol = 0;
    private int currentChip = 0;
    private int inputDelay;
    private int[][] chips;
    private WinningLine winningLine = new WinningLine();
    private boolean winner = false;

    private SpriteBatch batch;
    private Texture slot;
    private Texture chip_red;
    private Texture chip_yellow;

    private Label debugLabel;
    private Label debugBoardLayout;
    private Label winnerLabel;
    private TextButton buttonReset;
    private TextButton buttonQuit;

    public GameScreen(MyGame game) {
        super(game);
    }

    @Override
    protected void create() {
        batch = new SpriteBatch();

        chips = new int[cols][rows];
        fillChipsArray();
        loadState();

        slot = game.getAssetManager().get("board/slot.png", Texture.class);
        chip_red = game.getAssetManager().get("board/chip_red.png", Texture.class);
        chip_yellow = game.getAssetManager().get("board/chip_yellow.png", Texture.class);

        createUI();
    }

    private void fillChipsArray() {
        winner = false;
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                chips[col][row] = -1;
            }
        }
    }

    private void createUI() {
        buttonReset = new TextButton("Neustart", game.getSkin());
        buttonReset.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                fillChipsArray();
                currentCol = 0;
                currentChip = 0;
            }
        });

        buttonQuit = new TextButton("Menü", game.getSkin());
        buttonQuit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });

        MyUtils.fixActorSizes(buttonReset, buttonQuit);

        winnerLabel = new Label("", game.getSkin(), "winner");

        debugLabel = new Label("", game.getSkin());
        debugLabel.setColor(Color.BLACK);
        debugBoardLayout = new Label("", game.getSkin());
        debugBoardLayout.setColor(Color.BLACK);

        stage.addActor(debugLabel);
        stage.addActor(debugBoardLayout);
        stage.addActor(winnerLabel);
        stage.addActor(buttonReset);
        stage.addActor(buttonQuit);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.85f, .85f, .85f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!winner) {
            handleInput();
            checkForWinner();
        }

        renderBoard();
        renderUi();
    }

    private void handleInput() {
        if (inputDelay == 0) {
            boolean input = false;

            // move next chip
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                if (currentCol + 1 < cols) {
                    currentCol++;
                } else {
                    currentCol = 0;
                }
                input = true;
            } else if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                if (currentCol - 1 >= 0) {
                    currentCol--;
                } else {
                    currentCol = cols - 1;
                }
                input = true;
            }

            // drop chip
            else if (Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
                if (getChipAmount(currentCol) < rows - 1) {
                    chips[currentCol][getChipAmount(currentCol)] = currentChip;
                    currentCol = 0;
                    currentChip = 1 - currentChip;
                    input = true;
                }
            }

            if (input) inputDelay = 10;
        } else {
            inputDelay--;
        }
    }

    private void renderBoard() {
        batch.setProjectionMatrix(stage.getCamera().combined);
        batch.begin();

        // draw board
        float drawSize = 1f * Gdx.graphics.getHeight() / rows;
        float offset = (Gdx.graphics.getWidth() - Gdx.graphics.getHeight()) / 2f;
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows - 1; row++) {
                float x = col * drawSize;
                float y = row * drawSize;
                batch.draw(slot, offset + x, y, drawSize, drawSize);
            }
        }
        if (!winner) {
            batch.draw(getChipTexture(), offset + currentCol * drawSize, (rows - 1) * drawSize, drawSize, drawSize);
        }

        // draw dropped chips
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < chips[col].length; row++) {
                int c = chips[col][row];
                if (c == -1) continue;
                Texture chip = chips[col][row] == 0 ? chip_red : chip_yellow;
                batch.draw(chip, offset + col * drawSize, row * drawSize, drawSize, drawSize);
            }
        }

        batch.end();
    }

    private void renderUi() {
        // Debug stuff
        if (stage.isDebugAll()) {
            String debugText = String.format(
                    "Column: %s\nChip: %s\nChips in Column: %s\nWinner: %b",
                    currentCol, currentChip, getChipAmount(currentCol), winner
            );
            debugLabel.setText(debugText);

            StringBuilder debugBoardText = new StringBuilder();
            for (int row = rows - 2; row >= 0; row--) {
                for (int col = 0; col < cols; col++) {
                    debugBoardText.append(String.format("%3d", chips[col][row]));
                }
                debugBoardText.append("\n");
            }
            debugBoardLayout.setText(debugBoardText);
        } else {
            debugBoardLayout.setText("");
            debugLabel.setText("");
        }

        if (winner) {
            String winner = winningLine.getChipType() == 0 ? "Rot" : "Gelb";
            winnerLabel.setText(winner + " hat gewonnen.");
        } else {
            winnerLabel.setText("");
        }

        packWidgets();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);

        debugLabel.setPosition(25, height - debugLabel.getHeight() - 75);
        debugBoardLayout.setPosition(0, 75);

        winnerLabel.setPosition(
                (width - winnerLabel.getWidth()) / 2,
                height - winnerLabel.getHeight() - 25
        );

        buttonReset.setPosition(
                width - buttonReset.getWidth() - 25,
                height - buttonReset.getHeight() - 25
        );
        buttonQuit.setPosition(
                width - buttonQuit.getWidth() - 25,
                buttonReset.getY() - buttonQuit.getHeight() - 25
        );
    }

    @Override
    public void hide() {
        saveState();
        super.hide();
    }

    @Override
    public void dispose() {
        batch.dispose();
        super.dispose();
    }

    private Texture getChipTexture() {
        return currentChip == 0 ? chip_red : chip_yellow;
    }

    private int getChipAmount(int col) {
        return (int) IntStream.of(chips[col])
                .filter(i -> i != -1)
                .count();
    }

    private void checkForWinner() {
        // X,Y loop checking upwards
        outer: for (int col = 0; col < cols - 3; col++) {
            for (int row = 0; row < rows - 3; row++) {
                int type = chips[col][row];
                if (type == -1) continue; // skip empty slots

                // horizontal: X is winner
                // Y Y Y Y
                // X X X X
                // Y Y Y Y
                // Y Y Y Y
                if (chips[col + 1][row] == type && chips[col + 2][row] == type && chips[col + 3][row] == type) {
                    winningLine.setStart(col, row);
                    winningLine.setEnd(col + 3, row);
                    winner = true;
                }

                // diagonal: X is winner
                // Y Y Y X
                // Y Y X Y
                // Y X Y Y
                // X Y Y Y
                else if (chips[col + 1][row + 1] == type && chips[col + 2][row + 2] == type && chips[col + 3][row + 3] == type) {
                    winningLine.setStart(col, row);
                    winningLine.setEnd(col + 3, row + 3);
                    winner = true;
                }

                // vertical: X is winner
                // X Y Y Y
                // X Y Y Y
                // X Y Y Y
                // X Y Y Y
                else if (chips[col][row + 1] == type && chips[col][row + 2] == type && chips[col][row + 3] == type) {
                    winningLine.setStart(col, row);
                    winningLine.setEnd(col, row + 3);
                    winner = true;
                }

                // diagonal: X is winner
                // X Y Y Y
                // Y X Y Y
                // Y Y X Y
                // Y Y Y X
                else if (row >= 3 && (
                        chips[col + 1][row - 1] == type && chips[col + 2][row - 2] == type && chips[col + 3][row - 3] == type
                )) {
                    winningLine.setStart(col, row);
                    winningLine.setEnd(col + 3, row + 3);
                    winner = true;
                }

                // break out if a winner was found
                if (winner) {
                    winningLine.setChipType(type);
                    break outer;
                }
            }
        }
    }

    private FileHandle getSaveFile() {
        return Gdx.files.local("state.sav");
    }

    private void loadState() {
        if (getSaveFile().exists()) {
            try {
                ObjectInputStream ois = new ObjectInputStream(getSaveFile().read());
                chips = (int[][]) ois.readObject();
                ois.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveState() {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(getSaveFile().write(false));
            oos.writeObject(chips);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
