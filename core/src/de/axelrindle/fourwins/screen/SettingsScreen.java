package de.axelrindle.fourwins.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import de.axelrindle.fourwins.MyGame;
import de.axelrindle.fourwins.MyUtils;
import de.axelrindle.fourwins.ui.CustomizableSelectBox;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Comparator;

public class SettingsScreen extends BasicScreen {

    private Preferences preferences;

    private Label title;
    private TextButton buttonMenu;

    private CustomizableSelectBox<DisplayMode> resolutionSelectBox;
    private CheckBox fullscreenCheckbox;

    public SettingsScreen(MyGame game) {
        super(game);
    }

    @Override
    protected void create() {
        preferences = Gdx.app.getPreferences("de.axelrindle.libgdxlearning");

        title = new Label("Einstellungen", game.getSkin(), "title");

        createSettingsUI();

        buttonMenu = new TextButton("Menü", game.getSkin());
        buttonMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
            }
        });

        MyUtils.fixActorSizes(resolutionSelectBox, buttonMenu);

        stage.addActor(title);
        stage.addActor(buttonMenu);
        stage.addActor(resolutionSelectBox);
        stage.addActor(fullscreenCheckbox);
    }

    private void createSettingsUI() {
        // Display & Resolution
        resolutionSelectBox = new CustomizableSelectBox<>(game.getSkin());
        resolutionSelectBox.setAlignment(Align.center);
        resolutionSelectBox.setToStringFunction(displayMode ->
                displayMode.width + "x" + displayMode.height);
        resolutionSelectBox.setItems(getDisplayModes());
        resolutionSelectBox.setSelectedIndex(getSelectedDisplayMode(resolutionSelectBox));
        resolutionSelectBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                preferences.putString("resolution", resolutionSelectBox.getToStringFunction().apply(resolutionSelectBox.getSelected()));
                updateResolution();
            }
        });

        // Fullscreen
        fullscreenCheckbox = new CheckBox("Vollbild", game.getSkin());
        fullscreenCheckbox.setProgrammaticChangeEvents(false);
        fullscreenCheckbox.setChecked(Gdx.graphics.isFullscreen());
        fullscreenCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                preferences.putBoolean("fullscreen", fullscreenCheckbox.isChecked());
                updateResolution();
            }
        });
    }

    /**
     * Creates an array of unique {@link DisplayMode}s, where the Hz rate is
     * filtered by the current monitor's Hz rate.
     *
     * @return An array.
     */
    @NotNull
    private DisplayMode[] getDisplayModes() {
        DisplayMode mode = Gdx.graphics.getDisplayMode();
        return Arrays.stream(Gdx.graphics.getDisplayModes())
                .filter(it -> it.refreshRate == mode.refreshRate)
                .filter(it -> it.width >= 1024) // minimum required resolution is 1024px as width
                .sorted(Comparator.comparingInt(o -> o.width))
                .toArray(DisplayMode[]::new);
    }

    /**
     * Finds the index of the currently selected {@link DisplayMode}.
     *
     * @param displays The {@link CustomizableSelectBox}
     * @return The index, or -1 when not found.
     */
    private int getSelectedDisplayMode(@NotNull CustomizableSelectBox<DisplayMode> displays) {
        DisplayMode mode = Gdx.graphics.getDisplayMode();
        Array<DisplayMode> items = displays.getList().getItems();
        for (int i = 0; i < items.size; i++) {
            DisplayMode item = items.get(i);
            if (item.height == mode.height && item.width == mode.width) {
                return i;
            }
        }
        return -1;
    }

    private void updateResolution() {
        DisplayMode displayMode = resolutionSelectBox.getSelected();
        boolean fullscreen = fullscreenCheckbox.isChecked();

        if (fullscreen) {
            Gdx.graphics.setFullscreenMode(displayMode);
        } else {
            Gdx.graphics.setWindowedMode(displayMode.width, displayMode.height);
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        title.setPosition(
                (width - title.getWidth()) / 2,
                height - title.getHeight() * 5
        );

        resolutionSelectBox.setPosition(
                (width - resolutionSelectBox.getWidth()) / 2,
                (height - resolutionSelectBox.getHeight()) / 2
        );
        fullscreenCheckbox.setPosition(
                (width - fullscreenCheckbox.getWidth()) / 2,
                resolutionSelectBox.getY() - fullscreenCheckbox.getHeight() - 25
        );

        buttonMenu.setPosition(
                width - buttonMenu.getWidth() - 25,
                25
        );
    }

    @Override
    public void dispose() {
        super.dispose();
        preferences.flush();
    }
}
