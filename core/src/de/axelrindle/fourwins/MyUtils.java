package de.axelrindle.fourwins;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import org.jetbrains.annotations.NotNull;

public class MyUtils {

    public static void fixActorSize(Actor actor, int gap) {
        if (actor instanceof Widget) {
            Widget w = (Widget) actor;
            w.pack();
        }
        actor.setWidth(actor.getWidth() + gap);
        actor.setHeight(actor.getHeight() + gap);
    }

    public static void fixActorSize(Actor actor) {
        fixActorSize(actor, 35);
    }

    public static void fixActorSizes(int gap, @NotNull Actor... actors) {
        for (Actor button : actors) {
            fixActorSize(button, gap);
        }
    }

    public static void fixActorSizes(Actor... actors) {
        fixActorSizes(35, actors);
    }
}
