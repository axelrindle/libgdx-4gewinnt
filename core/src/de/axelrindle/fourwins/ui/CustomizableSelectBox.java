package de.axelrindle.fourwins.ui;

import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.function.Function;

public class CustomizableSelectBox<T> extends SelectBox<T> {

    private Function<T, String> toStringFunction = Object::toString;

    public CustomizableSelectBox(Skin skin) {
        super(skin);
    }

    public Function<T, String> getToStringFunction() {
        return toStringFunction;
    }

    public void setToStringFunction(Function<T, String> toStringFunction) {
        this.toStringFunction = toStringFunction;
    }

    @Override
    protected String toString(T item) {
        return toStringFunction.apply(item);
    }
}
