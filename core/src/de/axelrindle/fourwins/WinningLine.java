package de.axelrindle.fourwins;

import com.badlogic.gdx.math.Vector2;

public class WinningLine {

    private int chipType = -1;
    private Vector2 start = new Vector2();
    private Vector2 end = new Vector2();

    public int getChipType() {
        return chipType;
    }

    public void setChipType(int chipType) {
        if (chipType < 0 || chipType > 1)
            throw new IllegalArgumentException("chipType must be 0 or 1!");
        this.chipType = chipType;
    }

    public Vector2 getStart() {
        return start;
    }

    public void setStart(float x, float y) {
        start.set(x, y);
    }

    public Vector2 getEnd() {
        return end;
    }

    public void setEnd(float x, float y) {
        end.set(x, y);
    }
}
